#!/usr/bin/python3 -W ignore
import argparse
import pickle
import distances
import reciprocal_lca
import math
from timeit import default_timer as timer


def restricted_float(x):
    try:
        x = float(x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r not a floating-point literal" % (x,))

    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r not in range (0.0, 1.0)" % (x,))
    return x


def main():
    parser = argparse.ArgumentParser(description='Routing tree inference and link performance parameters estimation.')
    parser.add_argument('-v', '--verbose', action='store_true', help='increase output verbosity' )
    parser.add_argument('-t', '--type', required=True, choices=['loss', 'jitter', 'loss-sequences'], help='specify type of performance parameter')
    parser.add_argument('-tp', '--topology', choices=['physical', 'logical'], default='logical', help='choose physical or logical rooting tree')
    parser.add_argument('-r', '--reduction-formula', choices=['single', 'complete', 'average', 'weighted', 'generic'], default='weighted', help='choose formula employed in the LCA matrix reduction')
    parser.add_argument('-d', '--dissimilarity', choices=['jaccard', 'hamming', 'rogerstanimoto', 'dice'], default='jaccard', help='choose dissimilarity measure for binary sequences')
    parser.add_argument('-sf', '--src-file', required=True, help='specify capture text file of source node')
    parser.add_argument('-df', '--dst-file', required=True, nargs='+', help='specify capture text file(s) of destination node(s)')
    parser.add_argument('-i', '--image', default='topology.png', help='save inferred logical routing tree to image')
    parser.add_argument('-o', '--output', default='routing_tree.bin', help='save inferred tree object to binary file')
    parser.add_argument('-c', '--threshold', default=1e-9, help='choose threshold used in tree pruning')
    parser.add_argument('-a', '--alpha', type=restricted_float, default=None, help='choose alpha parameter for generic reduction formula')
    parser.add_argument('-p', '--pruning', action='store_true', help='enable tree pruning' )
    args = parser.parse_args()
    #print(args)

    if args.reduction_formula == 'generic' and args.alpha is None:
        parser.error("The generic reduction update formula requires alpha parameter.")

    ttls = dict() # dictionary to hold the TTL values - {nodeID: TTL}
    if args.topology == 'physical':
        for index, item in enumerate([args.src_file] + args.dst_file):
            ttls[index] = distances.get_ttl(item)
    else:
        ttls = None
        
    if args.type == 'loss':
        # source ID is assumed 0 and the IDs of the destination nodes follow in ascending order
        dist_mat = distances.estimate_loss_distances(args.src_file, {k+1: v for k, v in enumerate(args.dst_file)})
    elif args.type == 'jitter':
        dist_mat = distances.estimate_jitter_distances(args.src_file, {k+1: v for k, v in enumerate(args.dst_file)})
    else:
        dist_mat, seq_dict = distances.estimate_loss_seq_distances(args.src_file, {k+1: v for k, v in enumerate(args.dst_file)}, args.dissimilarity)

    if args.verbose:
        print('\nMatrix of distances between terminal nodes:\n')
        print(dist_mat)

    threshold = float(args.threshold)
    if args.type == 'loss-sequences':
        tree = reciprocal_lca.rec_lca_seq(dist_mat, seq_dict, threshold, ttls, formula=args.dissimilarity, verbose=args.verbose, pruning=args.pruning)
    else:
        tree = reciprocal_lca.rec_lca(dist_mat, threshold, ttls, args.alpha, formula=args.reduction_formula, verbose=args.verbose, pruning=args.pruning)

    # save topology to image
    tree.draw(args.image)

    # save to binary file
    with open(args.output, 'wb') as f:
        # Pickle the tree object
        pickle.dump(tree, f)

    f = open('estimations.txt', 'w')
    if args.type == 'jitter':
        for s, r, l in sorted(tree.get_edges(), key=lambda x: (x[0].get_ID(), x[1].get_ID())):
            f.write('Edge: {} --- {} [Length: {:.4f}] [Jitter (ms): {:.3f}]\n'.format(s.get_ID(), r.get_ID(), l, math.sqrt(l)))
    elif args.type == 'loss':
        for s, r, l in sorted(tree.get_edges(), key=lambda x: (x[0].get_ID(), x[1].get_ID())):
            f.write('Edge: {} --- {} [Length: {:.4f}] [Loss Rate (%): {:.3%}]\n'.format(s.get_ID(), r.get_ID(), l, 1-10**(-l)))
    else:
        for s, r, l in sorted(tree.get_edges(), key=lambda x: (x[0].get_ID(), x[1].get_ID())):
            f.write('Edge: {} --- {} [Length: {:.4f}]\n'.format(s.get_ID(), r.get_ID(), l))
    f.close()

    if args.verbose:
        print('\n' + args.topology.capitalize() + ' Routing Tree:')
        print('\nNodes:', [n.get_ID() for n in tree.get_nodes()])
        if args.type == 'jitter':
            for s, r, l in sorted(tree.get_edges(), key=lambda x: (x[0].get_ID(), x[1].get_ID())):
                print('Edge: {} --- {} [Length: {:.4f}] [Jitter (ms): {:.3f}]'.format(s.get_ID(), r.get_ID(), l, math.sqrt(l)))
        elif args.type == 'loss':
            for s, r, l in sorted(tree.get_edges(), key=lambda x: (x[0].get_ID(), x[1].get_ID())):
                print('Edge: {} --- {} [Length: {:.4f}] [Loss Rate (%): {:.3%}]'.format(s.get_ID(), r.get_ID(), l, 1-10**(-l)))
        else:
            for s, r, l in sorted(tree.get_edges(), key=lambda x: (x[0].get_ID(), x[1].get_ID())):
                print('Edge: {} --- {} [Length: {:.4f}]'.format(s.get_ID(), r.get_ID(), l))                
    print()

    
if __name__ == '__main__':
    start = timer()
    main()
    end = timer()
    print('Time elapsed: {:.4f} seconds'.format(end-start))