import pickle
import general_tree as gt
import sys
import re


def fix(file1, file2, last_dest, real_file):
    s = gt.Node(0) # create dummy source
    first_tree = gt.GeneralTree(s) # create tree
    second_tree = gt.GeneralTree(s)

    with open(file1, 'rb') as f1:
        first_tree = pickle.load(f1)

    with open(file2, 'rb') as f2:
        second_tree = pickle.load(f2)

    changes = list()
    
    for d in range(1, last_dest+1):
        node1 = first_tree.get_node(d)
        node2 = second_tree.get_node(d)
        i = node1
        j = node2
        while (i.ID != 0):
            if ((i.parent.ID) != (j.parent.ID)):
                changes.append((i.parent.ID, j.parent.ID))
            i = i.parent
            j = j.parent

    changes_dict = dict(set(changes))
    print(changes_dict)

    p = re.compile(r'(\d+)---(\d+)')

    f3 = open('new_' + real_file, 'w')

    with open(real_file, 'r') as f4:
        for line in f4:
            start = int(p.search(line).group(1))
            finish = int(p.search(line).group(2))
            line.split()[1]
            if start in changes_dict:
                start = changes_dict[start]
            if finish in changes_dict:
                finish = changes_dict[finish]

            new_line = str(start) + '---' + str(finish) + ' ' + line.split()[1] + '\n'
            f3.write(new_line)

    f3.close()



if __name__ == '__main__':
    file1 = sys.argv[1]
    file2 = sys.argv[2]
    last_dest = int(sys.argv[3])
    real_file = sys.argv[4]
    fix(file1, file2, last_dest, real_file)