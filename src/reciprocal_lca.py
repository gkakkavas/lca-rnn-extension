import numpy as np
import math
import stack as st
import general_tree as gt
import operator
from scipy.spatial.distance import jaccard, hamming, rogerstanimoto, kulsinski, russellrao, dice


def length_to_loss(tree):
    "Calculate link loss rate from corresponding link lenght"
    return [(s, r, 1-10**(-l)) for s, r, l in tree.get_edges()]


def length_to_jitter(tree):
    "Calculate link jitter (ms) from corresponding link lenght"
    return [(s, r, math.sqrt(l)) for s, r, l in tree.get_edges()]


def father_seq(s1, s2):
    """ Infer the outcome bit sequence of the father of two nodes.
        s1, s2: outcome bit sequences of the children """
    assert len(s1) == len(s2)
    return list(map(operator.or_, s1, s2))


def construct_lca_matrix(distances):
    dshape = distances.shape # shape of distances array
    lshape = (dshape[0]-1, dshape[1]-1) # shape of LCA array: less the source row and column
    # initialize lca matrix
    lca = np.zeros(lshape)
    # iterate over all indexes of lca matrix
    for i in range(lshape[0]):
        for j in range(i, lshape[1]):
            # source ID is assumed 0
            # i, j in lca matrix correspond to i+1, j+1 in original distances array
            lca[i,j] = max(0, (distances[0,i+1] + distances[0,j+1] - distances[i+1,j+1]) / 2)
            # LCA matrix is symmetric
            lca[j,i] = lca[i,j]
    return lca


def complete_chain(lca, mapping, nextid, chain):
    while True:
        try:
            top = chain.peek()
        except IndexError:
            # if stack is empty, start from the last created parent node
            chain.push(nextid-1)
            top = nextid-1 
        # lca index of node at the top of the stack
        i = mapping[top]
        # find lca index of maximal off-diagonal element of row i
        # maximal row element must be the diagonal element
        # assign a large value to break possible ties
        row = np.copy(lca[i,])
        row[i] = 10**6
        # find the index of the second maximal element
        j = np.argpartition(row, -2)[-2]
        # find respective node ID of maximal off-diagonal element
        nid = next(key for key, value in mapping.items() if value == j)
        # check if respective node ID is the penultimate element of the stack
        try:
            check = (nid == chain.peek(2))
        except IndexError:
            check = False
        if check: # if so terminate the chain
            break
        else: # else push the node ID in the stack
            chain.push(nid)
    #chain.print()
    r = chain.pop() # ID of one node
    l = chain.pop() # ID of other node
    return (l, r) # RNN pair: node IDs


def rec_lca(distances, threshold, ttls, a, formula='weighted', verbose=False, pruning=False):
    # dictionary that holds two different reduction formulas
    choices = { 'weighted': lambda x, y: (x+y)/2.0,
                'complete': lambda x, y: max(x,y),
                'single': lambda x, y: min(x,y) }

    #### Initialization ####
    # source ID is assumed 0 and the IDs of the destination nodes follow in ascending order
    s = gt.Node(0) # create source
    tree = gt.GeneralTree(s, nodes=None, leaves=[], edges=[]) # create tree
    for n in range(1, distances.shape[0]): # for every destination
        node = gt.Node(n) # create destination node
        tree.add_node(node) # add node to tree node set
        tree.add_leaf(node) # add node to tree leaves

    # create dictionary with cluster sizes - {nodeID: cluster_size}
    # we begin with each destination in each own cluster
    sizes = {n: 1 for n in range(1, distances.shape[0])}

    # find ID that will be used for the next created node (last used ID of the final destination node plus 1)
    nextid = tree.get_nodes()[-1].get_ID() + 1

    #### LCA depths matrix deduction ####
    lca = construct_lca_matrix(distances)
    if verbose:
        print('\nMatrix of LCA depths of destination nodes:\n')
        print(lca)

    ndest = lca.shape[0] # number of destination nodes
    # dictionary that holds the mapping of node IDs to lca matrix indexes - {nodeID: lca_index}
    mapping = {x: x-1 for x in range(1, ndest+1)}
    # initialize RNN chain: empty stack
    chain = st.Stack()
    # start the chain from node 1 (first destination node)
    chain.push(1)

    while True:
        #### Neighbor Selection ####
        lid, rid = complete_chain(lca, mapping, nextid, chain) # find reciprocal nearest neighbors
        # print("RNNs: ", lid, rid)

        # If we want the logical rooting tree or the pair of RNNs has the same TTL
        if ((not ttls) or ttls[lid] == ttls[rid]):
            #### Reduction ####
            l = tree.get_node(lid)
            r = tree.get_node(rid)
            # create node u as the parent of the RNN pair
            u = gt.Node(ID=nextid, children=[l,r])
            l.parent = u
            r.parent = u
            rindex = mapping[rid] # lca matrix index of node r
            lindex = mapping[lid] # lca matrix index of node l
            if (lindex <= rindex):
                kept = lindex # index of row and column where new lca values will be stored
                keptID = lid
                removed = rindex # index of row and column that will be removed from the lca matrix
                removedID = rid
            else:
                kept = rindex # index of row and column where new lca values will be stored
                keptID = rid
                removed = lindex # index of row and column that will be removed from the lca matrix
                removedID = lid
            # kept = min(lindex, rindex) 
            # removed = max(lindex, rindex) 
            # calculate distances between parent node u and nodes l, r
            dist_ul = max(0, lca[lindex,lindex] - lca[lindex, rindex])
            dist_ur = max(0, lca[rindex,rindex] - lca[lindex, rindex])
            # calculate lca values of newly created node
            dvals = dict() # dictionary that hold new lca distances
            if (formula == 'average'):
                for k in mapping.values():
                    if k != kept and k!= removed:
                        if k > removed:
                            # adjust indexes that are affected by removal of rindex
                            dvals[k-1] = (sizes[keptID] * lca[kept,k] + sizes[removedID] * lca[removed,k]) / (sizes[keptID] + sizes[removedID])
                        else:
                            dvals[k] = (sizes[keptID] * lca[kept,k] + sizes[removedID] * lca[removed,k]) / (sizes[keptID] + sizes[removedID])      
                    dvals[kept] =  lca[kept,removed]
            elif (formula == 'generic'):
                for k in mapping.values():
                    if k != kept and k!= removed:
                        if k > removed:
                            # adjust indexes that are affected by removal of rindex
                            dvals[k-1] = a*lca[kept,k] + (1-a)*lca[removed,k]
                        else:
                            dvals[k] = a*lca[kept,k] + (1-a)*lca[removed,k]      
                    dvals[kept] =  lca[kept,removed]
            else:
                for k in mapping.values():
                    if k != kept and k!= removed:
                        if k > removed:
                            # adjust indexes that are affected by removal of rindex
                            dvals[k-1] = choices[formula](lca[kept,k], lca[removed,k])
                        else:
                            dvals[k] = choices[formula](lca[kept,k], lca[removed,k])
                    dvals[kept] =  lca[kept,removed]

            vals = [dvals[k] for k in sorted(dvals)]
            # delete column removed from lca matrix
            lca = np.delete(lca, removed, 1)
            # delete  row removed from lca matrix
            lca = np.delete(lca, removed, 0)
            # replace row kept with a new row corresponding to newly created node u
            lca[kept] = vals
            # replace column kept with a new column
            lca[:, kept] = vals
            #print(lca)
            # update dictionnary mapping
            del mapping[lid]
            del mapping[rid]
            mapping[nextid] = kept
            # update mapping of nodes to indexes that are affected by deletion of removed
            for m in range(removed+1, ndest):
                mid = next(key for key, value in mapping.items() if value == m)
                mapping[mid] = mapping[mid]-1
            # dimensions of lca matrix are reduced by 1
            ndest = ndest - 1
            # update TTLs dictionary with parent node if necessary
            if ttls:
                ttls[nextid] = ttls[lid] + 1
            # update dictionary sizes
            sizes[nextid] = sizes[lid] + sizes[rid]
            del sizes[lid]
            del sizes[rid]
            # increment for next iteration
            nextid = nextid + 1

            #### Tree reconstruction ####
            # add parent node u to the set of tree nodes
            tree.add_node(u)
            # add two new edges from u to l and r after calculating their lengths
            tree.add_edge(u, l, dist_ul)
            tree.add_edge(u, r, dist_ur)

            #### Stopping condition ####
            if lca.size == 1:
                # connect the last remaining node with source s
                tree.add_edge(s, u, 0) # zero-length edge by design
                u.parent = s
                break  
        else:
            # the node with the smaller TTL has the largest depth
            if (ttls[lid] < ttls[rid]):
                lowID, highID = lid, rid
            else:
                lowID, highID = rid, lid
            
            l = tree.get_node(lowID)
            # Push the highest node back to the NN chain
            chain.push(highID)

            index = lowID
            # the difference in TTLs of the RNNs indicates the number of no branching points between them
            dif = ttls[highID] - ttls[lowID]
            for i in range(0, dif):
                # create node u as the parent of the low node
                u = gt.Node(ID=nextid, children=[l])
                l.parent = u
                lindex = mapping[index] # lca matrix index of node
                # update dictionnary mapping
                # node u takes the place of node index in lca matrix
                del mapping[index]
                mapping[nextid] = lindex
                index = nextid
                # increment for next iteration
                nextid = nextid + 1

                #### Tree reconstruction ####
                # add parent node u to the set of tree nodes
                tree.add_node(u)
                # add new edge from u to l with arbritrary large length
                # so that it won't get pruned later
                tree.add_edge(u, l, 1000)
            
            # Update TTLs dictionary - last parent node has same TTL with node highID
            ttls[nextid-1] = ttls[highID]
            # Update sizes dictionary - last parent node has same size with node lowID
            sizes[nextid-1] = sizes[lowID]
            del sizes[lowID]

    #### Turn binary tree into a general tree ####
    if pruning:
        # find edges with zero length, besides the one that starts from the source
        for s, d, l in [e for e in tree.get_edges() if e[2] < threshold and e[0].get_ID() != 0]:
            # remove the terminal node of the edge from the set of tree nodes
            tree.nodes.remove(d)
            # remove the edge from the set of tree edges
            tree.edges.remove((s, d, l))
            # remove the terminal node of the edge from the list of children of the starting node of the edge
            s.children.remove(d)
            # add the children of the terminal node of the edge to the list of children of the starting node of the edge
            s.children.extend(d.children)
            # correct the edges between the ending node and each of his children by making them start from the starting node
            for c in d.children:
                i = next(index for index, item in enumerate(tree.get_edges()) if item[0] == d and item[1] == c)
                _, _, length = tree.edges.pop(i)
                tree.edges.insert(i, (s, c, length))
                c.parent = s # set parent of former d children to s
            del d

    return tree


def rec_lca_seq(distances, sequences, threshold, ttls, formula, verbose=False, pruning=False):
    # dictionary that holds different dissimilarity functions
    choices = { 'jaccard': jaccard,
                'hamming': hamming,
                'rogerstanimoto': rogerstanimoto, 
                'kulsinski': kulsinski,
                'russellrao': russellrao,
                'dice': dice }
    #### Initialization ####
    # source ID is assumed 0 and the IDs of the destination nodes follow in ascending order
    s = gt.Node(0) # create source
    tree = gt.GeneralTree(s, nodes=None, leaves=[], edges=[]) # create tree
    for n in range(1, distances.shape[0]): # for every destination
        node = gt.Node(n) # create destination node
        tree.add_node(node) # add node to tree node set
        tree.add_leaf(node) # add node to tree leaves

    # find ID that will be used for the next created node (last used ID of the final destination node plus 1)
    nextid = tree.get_nodes()[-1].get_ID() + 1

    #### LCA depths matrix deduction ####
    lca = construct_lca_matrix(distances)
    if verbose:
        print('\nMatrix of LCA depths of destination nodes:\n')
        print(lca)

    ndest = lca.shape[0] # number of destination nodes
    # dictionary that holds the mapping of node IDs to lca matrix indexes - {nodeID: lca_index}
    mapping = {x: x-1 for x in range(1, ndest+1)}
    # initialize RNN chain: empty stack
    chain = st.Stack()
    # start the chain from node 1 (first destination node)
    chain.push(1)

    while True:
        #### Neighbor Selection ####
        lid, rid = complete_chain(lca, mapping, nextid, chain) # find reciprocal nearest neighbors

        # If we want the logical rooting tree or the pair of RNNs has the same TTL
        if ((not ttls) or ttls[lid] == ttls[rid]):
            #### Reduction ####
            l = tree.get_node(lid)
            r = tree.get_node(rid)
            # create node u as the parent of the RNN pair
            u = gt.Node(ID=nextid, children=[l,r])
            l.parent = u
            r.parent = u
            rindex = mapping[rid] # lca matrix index of node r
            lindex = mapping[lid] # lca matrix index of node l
            kept = min(lindex, rindex) # index of row and column where new lca values will be stored
            removed = max(lindex, rindex) # index of row and column that will be removed from the lca matrix
            # calculate outcome sequence of parent node u from nodes l, r
            sequences[nextid] = father_seq(sequences[lid], sequences[rid])
            # calculate distances between parent node u and nodes l, r
            dist_ul = choices[formula](sequences[nextid], sequences[lid])
            dist_ur = choices[formula](sequences[nextid], sequences[rid])
            # calculate lca values of newly created node
            dvals = dict() # dictionary that hold new lca distances
            for w, k in mapping.items():
                if k != kept and k!= removed:
                    if k > removed:
                        # adjust indexes that are affected by removal of rindex
                        dvals[k-1] = (choices[formula](sequences[0], sequences[nextid]) + 
                                    choices[formula](sequences[0], sequences[w]) -
                                    choices[formula](sequences[nextid], sequences[w])) / 2.0 
                    else:
                        dvals[k] = (choices[formula](sequences[0], sequences[nextid]) + 
                                    choices[formula](sequences[0], sequences[w]) -
                                    choices[formula](sequences[nextid], sequences[w])) / 2.0
                dvals[kept] =  lca[kept,removed]
            vals = [dvals[k] for k in sorted(dvals)]
            # delete column removed from lca matrix
            lca = np.delete(lca, removed, 1)
            # delete  row removed from lca matrix
            lca = np.delete(lca, removed, 0)
            # replace row kept with a new row corresponding to newly created node u
            lca[kept] = vals
            # replace column kept with a new column
            lca[:, kept] = vals
            #print(lca)
            # update dictionnary mapping
            del mapping[lid]
            del mapping[rid]
            mapping[nextid] = kept
            # update mapping of nodes to indexes that are affected by deletion of removed
            for m in range(removed+1, ndest):
                mid = next(key for key, value in mapping.items() if value == m)
                mapping[mid] = mapping[mid]-1
            # dimensions of lca matrix are reduced by 1
            ndest = ndest - 1
            # update TTLs dictionary with parent node if necessary
            if ttls:
                ttls[nextid] = ttls[lid] + 1
            # increment for next iteration
            nextid = nextid + 1

            #### Tree reconstruction ####
            # add parent node u to the set of tree nodes
            tree.add_node(u)
            # add two new edges from u to l and r after calculating their lengths
            tree.add_edge(u, l, dist_ul)
            tree.add_edge(u, r, dist_ur)

            #### Stopping condition ####
            if lca.size == 1:
                # connect the last remaining node with source s
                tree.add_edge(s, u, 0) # zero-length edge by design
                u.parent = s
                break
        else:
            # the node with the smaller TTL has the largest depth
            if (ttls[lid] < ttls[rid]):
                lowID, highID = lid, rid
            else:
                lowID, highID = rid, lid
            
            l = tree.get_node(lowID)
            # Push the highest node back to the NN chain
            chain.push(highID)

            index = lowID
            # the difference in TTLs of the RNNs indicates the number of no branching points between them
            dif = ttls[highID] - ttls[lowID]
            for i in range(0, dif):
                # create node u as the parent of the low node
                u = gt.Node(ID=nextid, children=[l])
                l.parent = u
                lindex = mapping[index] # lca matrix index of node
                # update dictionnary mapping
                # node u takes the place of node index in lca matrix
                del mapping[index]
                mapping[nextid] = lindex
                # copy binary sequency to parent node
                sequences[nextid] = sequences[index]
                index = nextid
                # increment for next iteration
                nextid = nextid + 1

                #### Tree reconstruction ####
                # add parent node u to the set of tree nodes
                tree.add_node(u)
                # add new edge from u to l with arbitrary large length
                # so that it won't get pruned later
                tree.add_edge(u, l, 1000)
            
            # Update TTLs dictionary - last parent node has same TTL with node highID
            ttls[nextid-1] = ttls[highID]
        
    #### Turn binary tree into a general tree ####
    if pruning:
        # find edges with zero length, besides the one that starts from the source
        for s, d, l in [e for e in tree.get_edges() if e[2] < threshold and e[0].get_ID() != 0]:
            # remove the terminal node of the edge from the set of tree nodes
            tree.nodes.remove(d)
            # remove the edge from the set of tree edges
            tree.edges.remove((s, d, l))
            # remove the terminal node of the edge from the list of children of the starting node of the edge
            s.children.remove(d)
            # add the children of the terminal node of the edge to the list of children of the starting node of the edge
            s.children.extend(d.children)
            # correct the edges between the ending node and each of his children by making them start from the starting node
            for c in d.children:
                i = next(index for index, item in enumerate(tree.get_edges()) if item[0] == d and item[1] == c)
                _, _, length = tree.edges.pop(i)
                tree.edges.insert(i, (s, c, length))
                c.parent = s # set parent of former d children to s
            del d

    return tree


if __name__ == '__main__':
    ########### TESTING ###########
    distances = np.array([[0,4,9,11,10,13], [4,0,11,13,12,15], [9,11,0,6,5,8], 
                          [11,13,6,0,7,10], [10,12,5,7,0,9], [13,15,8,10,9,0]])

    tree = rec_lca(distances, 0.01, None, None, verbose=True, pruning=False)
    tree.draw('joined.png')
    print()
    print('Nodes:', [n.get_ID() for n in tree.get_nodes()])
    for s, r, l in tree.get_edges():
        print('Edge: {} --- {} [Length: {}]'.format(s.get_ID(), r.get_ID(), l))
