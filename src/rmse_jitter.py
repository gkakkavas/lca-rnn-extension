import sys
import re
import math

def calc_rmse(filename1, filename2):
    p = re.compile(r'Edge:\s(\d+\s---\s\d+)')
    q = re.compile(r'\(ms\):\s(\d+\.\d+)')
    est = dict()
    real = dict()
    with open(filename1, 'r') as f:
        for line in f:
            key = line.split()[0]
            value = float(line.split()[1])
            real[key] = value
    with open(filename2, 'r') as f:
        for line in f:
            key = p.search(line).group(1).replace(" ", "")
            value = float(q.search(line).group(1))
            est[key] = value

    squares = {k: round((real[k]-est[k])**2, 6) for k in real}# if k in est}
    #print(squares)
    mse = sum(squares.values()) / (len(squares)-1)
    print(mse)
    return math.sqrt(mse)


if __name__ == '__main__':
    res = calc_rmse(sys.argv[1], sys.argv[2])
    print("Calculated RMSE is: {:.4f} ".format(res))
