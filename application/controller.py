import zmq
from termcolor import colored
from collections import defaultdict
import time
import os
import subprocess
import csv
import configparser
import numpy as np
import sys
sys.path.append('../src')
import distances
import reciprocal_lca


def draw(rng, mu, phi):
    alpha = phi * mu
    beta = phi * (1 - mu)
    return rng.beta(alpha, beta)


def zip_equal(list1, list2):
    if len(list1) != len(list2):
        raise ValueError("Lengths of lists are different!")
    return zip(list1, list2)


def main(NT_type, iterations, tnn, nodes, cps, lnum, paths, mapping):
    context = zmq.Context()
    rng = np.random.default_rng()
    # Holds mean loss rates of internal links {'node': array([lval, rval])}
    # or {'node': val}
    means = dict()

    print(STD_INFO + "Connecting to internal nodes and initialize loss rates…")
    in_socket = context.socket(zmq.REQ)
    for n in range(tnn, len(nodes)):
        in_socket.connect("tcp://%s:%s" %
                          (nodes[str(n)][0].split()[0], nodes[str(n)][1]))
        # Initialize mean loss rates of internal links in the range 0.05 to 0.2
        if len(nodes[str(n)][0].split()) == 2:
            means[str(n)] = rng.uniform(0.05, 0.2, 2)
        else:
            means[str(n)] = rng.uniform(0.05, 0.2)

    #  order: destination1, destination2, ..., source
    print(STD_INFO + "Connecting to terminal nodes…")
    socket = context.socket(zmq.REQ)
    for n in range(1, tnn):
        socket.connect("tcp://%s:%s" % (nodes[str(n)][0], nodes[str(n)][1]))
    socket.connect("tcp://%s:%s" % (nodes['0'][0], nodes['0'][1]))
    
    iteration = 0
    rep = 0
    while iteration < iterations:
        try:
            # Holds the ground truth loss rate for every link: {link_i: loss_rate}
            gtruth_vals = dict()

            if iteration in cps:  # If we are at a change point
                change = True
            else:
                change = False

            for n in range(tnn, len(nodes)):  # For every internal node
                print(STD_INFO +
                      "Sending probability(-ies) to internal node %s" % n)

                # outgoing links of node
                outgoing = sorted(
                    filter(
                        lambda tup: tup[0] == str(n),
                        mapping.keys()))

                if len(outgoing) == 2:
                    if change:  # Change mean loss rates if we are at change point
                        means[str(n)] = rng.uniform(0.05, 0.2, 2)
                    # Draw from the respective beta distributions
                    val1 = draw(rng, means[str(n)][0], 10000)
                    val2 = draw(rng, means[str(n)][1], 10000)
                    probs = "%s %s" % (val1, val2)
                    gtruth_vals[mapping[outgoing[0]]] = val1
                    gtruth_vals[mapping[outgoing[1]]] = val2
                else:
                    if change:  # Change mean loss rate if we are at change point
                        means[str(n)] = rng.uniform(0.05, 0.2)
                    # Draw from the respective beta distribution
                    val1 = draw(rng, means[str(n)], 10000)
                    probs = str(val1)
                    gtruth_vals[mapping[outgoing[0]]] = val1

                in_socket.send_string(probs)

                #  Get the reply from internal node if message acknowledged.
                message = in_socket.recv_string()
                print(STD_INPUT + "Received reply [ %s ]" % message)

            gtruth_exists = os.path.isfile('ground_truth.csv')
            with open("ground_truth.csv", "a", newline='') as csvfile:
                gtruth_writer = csv.writer(
                    csvfile,
                    delimiter=',',
                    quotechar='"',
                    quoting=csv.QUOTE_MINIMAL)

                if not gtruth_exists:  # if file does not exist
                    gt_headers = ['link_' + str(i) for i in range(1, lnum)]
                    gtruth_writer.writerow(
                        ['iteration'] + gt_headers)  # write the header

                gtruth_writer.writerow([iteration] + [gtruth_vals[k]
                                       for k in sorted(gtruth_vals.keys())])

            # with open("means.txt", "a", newline='') as f:
            #     f.write(str(means)+'\n')

            for n in range(1, tnn):  # For every destination
                print(STD_INFO + "Sending request to destination %s" % str(n))
                socket.send_string("Destination %s - Start processes" % str(n))
                #  Get the reply from destination node if message acknowledged.
                message = socket.recv_string()
                print(STD_INPUT + "Received reply [ %s ]" % message)

            # Send request to source
            print(STD_INFO + "Sending request to source")
            socket.send_string("Source - Start processes")
            # Get the txt capture file from source
            with open('source.txt', 'wb') as f:
                data = socket.recv()
                f.write(data)
            print(STD_INPUT + "Received txt capture file from source")

            # Get the txt capture files from destinations
            for n in range(1, tnn):
                print(
                    STD_INFO +
                    "Requesting txt capture file from destination " +
                    str(n))
                socket.send_string(
                    "Destination %s - Probing has finished" %
                    str(n))
                with open('dest' + str(n) + '.txt', 'wb') as f:
                    data = socket.recv()
                    f.write(data)
                print(
                    STD_INPUT +
                    "Received txt capture file from destination " +
                    str(n))

            # send request to source, so that at the next iteration
            # the first send will be addressed to destination 1
            socket.send_string("Source - Start new iteration")
            message = socket.recv_string()

            # file_exists = os.path.isfile('nprobes.csv')
            # with open("nprobes.csv", "a", newline='') as csvfile:
            #     headers = ['iteration', 'source.txt', 'dest1.txt', 'dest2.txt']
            #     res_writer = csv.writer(
            #         csvfile,
            #         delimiter=',',
            #         quotechar='"',
            #         quoting=csv.QUOTE_MINIMAL)
            #     r0 = subprocess.run(
            #         "wc -l source.txt",
            #         shell=True,
            #         capture_output=True,
            #         text=True)
            #     r1 = subprocess.run(
            #         "wc -l dest1.txt",
            #         shell=True,
            #         capture_output=True,
            #         text=True)
            #     r2 = subprocess.run(
            #         "wc -l dest2.txt",
            #         shell=True,
            #         capture_output=True,
            #         text=True)
            #     if not file_exists:  # if file does not exist
            #         res_writer.writerow(headers)  # write the header
            #     res_writer.writerow([iteration, r0.stdout.split()[
            # 0], r1.stdout.split()[0], r2.stdout.split()[0]])

            # TODO Network Tomography
            print(STD_TODO + 'Executing loss network tomography.')

            print(STD_TODO + 'Calculating distance matrix between terminal nodes.')

            try:
                dist_mat = distances.estimate_loss_distances(
                    'source.txt', {n: 'dest' + str(n) + '.txt' for n in range(1, tnn)})
            except:
                print(STD_ERROR + 'Bad iteration. Repeating…')
                rep += 1
                # Removing files
                for item in os.listdir():
                    if item.endswith('.txt'):
                        os.remove(item)
                time.sleep(1)
                continue

            print(
                STD_TODO +
                'Calculating matrix LCA depths matrix and performing clustering.')
            tree = reciprocal_lca.rec_lca(
                dist_mat,
                None,
                None,
                None,
                formula='weighted',
                verbose=False,
                pruning=False)

            estimate_exists = os.path.isfile('estimates.csv')
            with open('estimates.csv', 'a', newline='') as estfile:
                est_writer = csv.writer(
                    estfile,
                    delimiter=',',
                    quotechar='"',
                    quoting=csv.QUOTE_MINIMAL)

                if not estimate_exists:  # if file does not exist
                    gt_headers = ['link_' + str(i) for i in range(1, lnum)]
                    est_writer.writerow(
                        ['iteration'] + gt_headers)  # write the header

                print(
                    STD_TODO +
                    'Calculating loss rate estimates from link lengths.')
                # Holds the loss rate estimates {('s', 'e'): loss}
                estimates = dict()
                for s, e, l in tree.get_edges():
                    estimates[(str(s.get_ID()), str(e.get_ID()))] = (1 - 10**(-l))

                # Holds the estimates to be recorded {'link_i': loss}
                est_vals = dict()

                try:
                    for n in range(1, tnn):
                        i = tree.get_node(n)
                        est_path = list()
                        while (i.ID != 0):
                            est_path.append((str(i.parent.ID), str(i.ID)))
                            i = i.parent
                        for baseline, current in zip_equal(
                                paths[n], est_path[::-1]):
                            est_vals[mapping[baseline]] = estimates[current]
                except:
                    print(STD_ERROR + 'Bad iteration. Repeating…')
                    rep += 1
                    # Removing files
                    for item in os.listdir():
                        if item.endswith('.txt'):
                            os.remove(item)
                    time.sleep(1)
                    continue

                est_writer.writerow([iteration] + [est_vals[k]
                                    for k in sorted(est_vals.keys()) if k != 'link_0'])

            # Removing files
            for item in os.listdir():
                if item.endswith('.txt'):
                    os.remove(item)

            iteration += 1
            print(colored('#' * 20 + ' ITERATION FINISHED ' + '#' * 20, 'red'))

        except KeyboardInterrupt:
            print("\n" + STD_WARNING + "Interrupt received, stopping…")
            # Clean up
            socket.close()
            in_socket.close()
            context.term()
            for item in os.listdir():
                if item.endswith('.txt'):
                    os.remove(item)
            break

    print(STD_INFO + 'Number of repetitions: ' + str(rep))


if __name__ == "__main__":

    STD_INFO = colored('[INFO] ', 'green')
    STD_ERROR = colored('[ERROR] ', 'red')
    STD_WARNING = colored('[WARNING] ', 'yellow')
    STD_INPUT = colored('[INPUT] ', 'blue')
    STD_TODO = colored('[COMPUTE] ', 'magenta')

    if not os.path.isfile('config.ini'):
        print(STD_ERROR + "Missing configuration file config.ini")
    else:
        config = configparser.ConfigParser()
        config.read('config.ini')

        nodes = dict()  # {'node': (IP(s), port) or 'node': (IP, port, path)}
        iterations = config.getint('main', 'iterations')
        NT_type = config['main']['type']
        tnn = config.getint('main', 'terminal_nodes_no')
        lnum = config.getint('main', 'links_no')
        cps = [int(p) for p in config['main']['change_points'].split()]

        for section in config.sections()[1:]:  # Skip main section
            nid = config[section]['node_id']
            ip_address = config[section]['ip']
            port = config[section]['port']
            if section.startswith('destination'):
                path = config[section]['path_to_source']
                nodes[nid] = (ip_address, port, path)
            else:
                nodes[nid] = (ip_address, port)

        links = set()  # Set of links of the topology
        mapping = dict()  # Holds link labels {('start', 'end'): link_i}
        # Holds path to source for every destination 
        # {n: [('0', 'x'), ('x', 'y'), ...,('z', 'n')]}
        paths = defaultdict(list)

        for n in range(1, tnn):
            p = nodes[str(n)][2].split()[::-1]
            for first, second in zip(p, p[1:]):
                links.add((first, second))
                paths[n].append((first, second))

        for i, l in enumerate(sorted(links)):
            mapping[l] = 'link_' + str(i)

        main(NT_type, iterations, tnn, nodes, cps, lnum, paths, mapping)
        # print(paths)
        # print(mapping)
        # print(links)
