import time
import zmq
import sys
import socket as s
from termcolor import colored
import psutil
import subprocess
import shlex
import os


def main(port, interface, src_addr):
    capture_cmd = shlex.split('sudo tcpdump -nlvvv -i ' + interface + ' udp and port 5001')
    probe_cmd = shlex.split('iperf -s -u -B 239.1.2.3%' + interface + ' -H ' + src_addr)

    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:%s" % port)

    while True:
        print(STD_INFO + "Waiting for controller…")
        # SIGINT will normally raise a KeyboardInterrupt
        try:
            # Wait for next request from controller
            message = socket.recv_string()
            print(STD_INPUT + "Received request [ %s ]" % message)
            # Get node ID from message
            nid = message.split()[1]

            # Start iperf and tcpdump in new processes
            logfile = open("temp.txt", "w")

            p1 = subprocess.Popen(
                capture_cmd,
                stdout=logfile,
                stderr=subprocess.STDOUT,
                text=True
            )
            p2 = subprocess.Popen(
                probe_cmd,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL
            )

            #  Send reply back to client
            socket.send_string("OK from %s" % s.gethostname())

            print(STD_INFO + 'Waiting for probing to finish…')
            message = socket.recv_string()
            print(STD_INPUT + "Received request [ %s ]" % message)
            time.sleep(0.5)

            # Terminate processes
            print(STD_INFO + 'Terminating processes')
            subprocess.run(['sudo', 'kill', str(p1.pid)]) # belongs to root
            p2.terminate()
            logfile.close()

            # Remove fist line of txt file
            subprocess.run('tail -n +2 temp.txt > dest' + nid + '.txt', shell=True)

            # Send capture file to controller
            print(STD_INFO + 'Sending txt capture file to controller')
            with open('dest' + nid + '.txt', 'rb') as f:
                data = f.read()
                socket.send(data)

            # Removing files
            os.remove('temp.txt')
            os.remove('dest' + nid + '.txt')

            print(colored('#'*20 + ' ITERATION FINISHED ' + '#'*20, 'red'))

        except KeyboardInterrupt:
            print("\n" + STD_WARNING + "Interrupt received, stopping…")
            # Clean up
            socket.close()
            context.term()
            for item in os.listdir():
                if item.endswith('.txt'):
                    os.remove(item)
            break


if __name__ == '__main__':
    STD_INFO = colored('[INFO] ', 'green')
    STD_ERROR = colored('[ERROR] ', 'red')
    STD_WARNING = colored('[WARNING] ', 'yellow')
    STD_INPUT = colored('[INPUT] ', 'blue')

    if len(sys.argv) != 4:
        print(STD_ERROR + "Usage: python3 %s SRC_IP_ADDRESS IP_ADDRESS TCP_PORT" % sys.argv[0])
    else:
        # find NIC name
        addrs = psutil.net_if_addrs()
        for key, value in addrs.items():
            if value[0][1] == sys.argv[2]:
                interface = key
                break

        main(sys.argv[3], interface, sys.argv[1])