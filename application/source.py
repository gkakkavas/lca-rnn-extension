import subprocess
import shlex
import time
from termcolor import colored
import zmq
import socket as s
import sys
import psutil
import os


def main(port, interface):
    capture_cmd = shlex.split('sudo tcpdump -nlvvv -i ' + interface + ' udp and port 5001')
    probe_cmd = shlex.split('iperf -c 239.1.2.3 -u -T 32 -b 10m -t 6')

    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:%s" % port)

    while True:
        print(STD_INFO + "Waiting for controller…")
        # SIGINT will normally raise a KeyboardInterrupt
        try:
            # Wait for next request from controller
            message = socket.recv_string()
            print(STD_INPUT + "Received request [ %s ]" % message)
            print(STD_INFO + "Executing probing…")

            # Start iperf and tcpdump in new processes
            logfile = open("temp.txt", "w")

            p1 = subprocess.Popen(
                capture_cmd,
                stdout=logfile,
                stderr=subprocess.STDOUT,
                text=True
            )

            time.sleep(0.1)

            p2 = subprocess.Popen(
                probe_cmd,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL
            )

            while True:
                if p2.poll() is not None: # if probing has finished
                    time.sleep(1)
                    subprocess.run(['sudo', 'kill', str(p1.pid)]) # stop capturing
                    break

            logfile.close()

            # Remove fist line of txt file
            subprocess.run('tail -n +2 temp.txt > source.txt', shell=True)

            # Send capture file to controller
            print(STD_INFO + 'Sending txt capture file to controller')
            with open('source.txt', 'rb') as f:
                data = f.read()
                socket.send(data)

            # Removing files
            os.remove('temp.txt')
            os.remove('source.txt')

            message = socket.recv_string()
            socket.send_string("OK from %s" % s.gethostname())

            print(colored('#'*20 + ' ITERATION FINISHED ' + '#'*20, 'red'))

        except KeyboardInterrupt:
            print("\n" + STD_WARNING + "Interrupt received, stopping…")
            # Clean up
            socket.close()
            context.term()
            for item in os.listdir():
                if item.endswith('.txt'):
                    os.remove(item)
            break


if __name__ == '__main__':
    STD_INFO = colored('[INFO] ', 'green')
    STD_ERROR = colored('[ERROR] ', 'red')
    STD_WARNING = colored('[WARNING] ', 'yellow')
    STD_INPUT = colored('[INPUT] ', 'blue')

    if len(sys.argv) != 3:
        print(STD_ERROR + "Usage: python3 %s IP_ADDRESS TCP_PORT" % sys.argv[0])
    else:
        # find NIC name
        addrs = psutil.net_if_addrs()
        for key, value in addrs.items():
            if value[0][1] == sys.argv[1]:
                interface = key
                break

        main(sys.argv[2], interface)