# Motivating Detection Application

An application combining the LCA-RNN clustering algorithm with *change point analysis* for detecting performance anomalies.

## Prerequisites

The following libraries are required:

* PyZMQ
* Termcolor
* Psutil

## Example Topology

![](exp_topo.png)

## Usage

Provide the configuration of the experiment in the file *config.ini*

Run the following scripts at the respective nodes (inside the folder *application*):

```
$ python3 source.py IP_ADDRESS TCP_PORT
$ python3 destination.py SRC_IP_ADDRESS IP_ADDRESS TCP_PORT
$ python3 internal.py IP_ADDRESS_1 [IP_ADDRESS_2] TCP_PORT
$ python3 controller.py

$ python3 changepoint_detection.py CSV_FILENAME CHANGE_POINTS
```

