import pandas as pd
import matplotlib.pyplot as plt
import changefinder
import ruptures as rpt
import sys


def detect(filename, bkps):
    # Read csv file
    df = pd.read_csv(filename, index_col='iteration')
    print(df)
    # Convert to numpy array
    signal = df.to_numpy()
    print(signal)
    # rpt.display(signal, bkps)
    # plt.show()

    # Pelt search method
    algo = rpt.Pelt(model='rbf', min_size=1, jump=1).fit(signal)
    result = algo.predict(pen=0.3)
    # Display the results
    rpt.display(signal, bkps, result)
    plt.xlabel ("Iteration", fontsize=16, fontweight='bold')
    plt.tight_layout()
    plt.savefig('detection.png')
    print(result[:-1])


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: python3 %s CSV_FILENAME CHANGE_POINTS' % sys.argv[0])
        # python changepoint_detection.py test.csv '9 15 27 32 57 88 109 146 200 239'
    else:
        filename = sys.argv[1]
        cps = [int(i) for i in sys.argv[2].split()]
        detect(filename, cps)