import zmq
import subprocess
import shlex
import socket as s
from termcolor import colored
import psutil
import sys


def main(port, linterface, rinterface):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:%s" % port)

    print(STD_INFO + "Initializing iptables rules.")
    i1 = 'sudo iptables -A FORWARD -d 239.1.2.3 -o {} -m statistic --mode random --probability 0 -j DROP'.format(
        linterface)
    subprocess.run(i1, shell=True)

    if rinterface:
        i2 = 'sudo iptables -A FORWARD -d 239.1.2.3 -o {} -m statistic --mode random --probability 0 -j DROP'.format(
            rinterface)
        subprocess.run(i2, shell=True)

    while True:
        try:
            # try:
            message = socket.recv_string()
            # except zmq.error.ZMQError as e:
            #     print("Error %s" % e)
            #     raise KeyboardInterrupt
            print(STD_INPUT + "Received probability(-ies) [ %s ]" % message)
            # Get dropping probabilities from message
            try:
                lprob, rprob = message.split()
            except ValueError:  # there is only one probability
                lprob = message

            r1 = 'sudo iptables -R FORWARD 1 -d 239.1.2.3 -o {} -m statistic --mode random --probability {} -j DROP'.format(
                linterface, lprob)
            subprocess.run(r1, shell=True)

            if rinterface:
                r2 = 'sudo iptables -R FORWARD 2 -d 239.1.2.3 -o {} -m statistic --mode random --probability {} -j DROP'.format(
                    rinterface, rprob)
                subprocess.run(r2, shell=True)

            #  Send reply back to controller
            print(STD_INFO + "Acknowledging iptables' rules update.")
            socket.send_string("Rules updated from %s" % s.gethostname())

            print(colored('#' * 20 + ' ITERATION FINISHED ' + '#' * 20, 'red'))

        except KeyboardInterrupt:
            print("\n" + STD_WARNING + "Interrupt received, stopping…")
            # Clean up
            socket.close()
            context.term()
            # Flush rules
            subprocess.run('sudo iptables -F FORWARD', shell=True)
            break


if __name__ == '__main__':
    STD_INFO = colored('[INFO] ', 'green')
    STD_ERROR = colored('[ERROR] ', 'red')
    STD_WARNING = colored('[WARNING] ', 'yellow')
    STD_INPUT = colored('[INPUT] ', 'blue')
    addrs = psutil.net_if_addrs()

    if len(sys.argv) == 3:  # internal node with one child
        # find NIC name
        for key, value in addrs.items():
            if value[0][1] == sys.argv[1]:
                linterface = key
                break
        main(sys.argv[3], linterface, None)
    elif len(sys.argv) == 4:  # internal node with two children
        # find NIC names
        for key, value in addrs.items():
            if value[0][1] == sys.argv[1]:
                linterface = key
            if value[0][1] == sys.argv[2]:
                rinterface = key
        main(sys.argv[3], linterface, rinterface)
    else:
        print(
            STD_ERROR +
            "Usage: python3 %s IP_ADDRESS_1 [IP_ADDRESS_2] TCP_PORT" %
            sys.argv[0])
