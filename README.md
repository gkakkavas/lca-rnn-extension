# Inferential network monitoring

An agglomerative clustering algorithm for routing tree inference and link parameters estimation based on end-to-end multicast measurements from a source node to a set of destination nodes. For more details see:

G. Kakkavas, V. Karyotis, and S. Papavassiliou, "Topology Inference and Link Parameter Estimation Based on End-to-End Measurements," Future Internet, vol. 14, no. 2, p. 45, Jan. 2022, doi: [10.3390/fi14020045](https://dx.doi.org/10.3390/fi14020045).

## Prerequisites

This project has been developed and tested in Python 3.8.6 and requires the following libraries:

- NumPy
- SciPy
- Matplotlib
- NetworkX
- pydot

## Usage

The tcpdump capture text files must be produced with the verbose option (-vvv). The source ID is assumed 0 and the IDs of the destination nodes follow in ascending order:

```
usage: tomography.py [-h] [-v] -t {loss,jitter,loss-sequences} [-tp {physical,logical}]
                     [-r {single,complete,average,weighted,generic}]
                     [-d {jaccard,hamming,rogerstanimoto,dice}] -sf SRC_FILE -df DST_FILE [DST_FILE ...]
                     [-i IMAGE] [-o OUTPUT] [-c THRESHOLD] [-a ALPHA] [-p]

Routing tree inference and link performance parameters estimation.

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output verbosity
  -t {loss,jitter,loss-sequences}, --type {loss,jitter,loss-sequences}
                        specify type of performance parameter
  -tp {physical,logical}, --topology {physical,logical}
                        choose physical or logical rooting tree
  -r {single,complete,average,weighted,generic}, --reduction-formula {single,complete,average,weighted,generic}
                        choose formula employed in the LCA matrix reduction
  -d {jaccard,hamming,rogerstanimoto,dice}, --dissimilarity {jaccard,hamming,rogerstanimoto,dice}
                        choose dissimilarity measure for binary sequences
  -sf SRC_FILE, --src-file SRC_FILE
                        specify capture text file of source node
  -df DST_FILE [DST_FILE ...], --dst-file DST_FILE [DST_FILE ...]
                        specify capture text file(s) of destination node(s)
  -i IMAGE, --image IMAGE
                        save inferred logical routing tree to image
  -o OUTPUT, --output OUTPUT
                        save inferred tree object to binary file
  -c THRESHOLD, --threshold THRESHOLD
                        choose threshold used in tree pruning
  -a ALPHA, --alpha ALPHA
                        choose alpha parameter for generic reduction formula
  -p, --pruning         enable tree pruning

```

## Example

```
$ cd example
$ python3 ../src/tomography.py -v -p -t loss -r weighted -c 0.01 -sf source0_5k.txt -df dest{1..17}_5k.txt
```

<a href="https://asciinema.org/a/379329" target="_blank"><img src="https://asciinema.org/a/379329.svg" width="800" height="486"/></a>

## Performance Anomaly Detection Application

See [detailed description](application/README.md).

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
